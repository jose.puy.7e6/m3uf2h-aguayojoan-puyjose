﻿using System;
using System.Collections.Generic;

namespace Exercici4
{
    class Analisi
    {
        static void Main()
        {
            string input;
            float maxims;
            float minims;
            float mitjana;
            int total;
            List<float> temperatures = new List<float>();

            do
            {
                string[] valors = Console.ReadLine().Split(" ");
                input = valors[0];

                switch (input)
                {
                    case "BOLCATDADES":
                        BolcatDades(temperatures, valors);
                        break;
                    case "MITJANA":
                        Console.WriteLine("La mitjana és de " + (Mitjana(temperatures) - 273.15) + "ºC");
                        break;
                    case "MAXIMS":
                        Console.WriteLine("La temperatua maxima és " + (Maxims(temperatures) - 273.15) + "ºC");
                        break;
                    case "MINIMS":
                        Console.WriteLine("La temperatua minima és " + (Minims(temperatures) - 273.15) + "ºC");
                        break;
                    case "ALTES":
                        if (valors[1] == null) Altes(temperatures);
                        else AltesX(temperatures, valors[1]);
                        break;
                    case "BAIXES":
                        Baixes(temperatures);
                        break;
                    case "FARENHEIT":
                        Farenheit(temperatures);
                        break;
                    case "KELVIN":
                        Kelvin(temperatures);
                        break;
                }
            } while (input != "EXIT");
        }

        static void BolcatDades(List<float> temperatures, string[] valors)
        {
            Console.WriteLine("Introdueix les dades, separades per espais.");

            double[] swap = new double[valors.Length];
            for (int i = 1; i < valors.Length; i++)
            {
                swap[i] = float.Parse(valors[i]);
            }
            foreach (double num in swap)
            {
                temperatures.Add((float)num);
            }
        }

        static float Mitjana(List<float> temperatures)
        {
            float suma = 0;
            int counter = 0;
            foreach (float num in temperatures)
            {
                suma += num;
                counter++;
            }
            return suma / counter;
        }

        static float Maxims(List<float> temperatures)
        {
            float maxim = temperatures[0];
            foreach (float num in temperatures)
            {
                if (num > maxim)
                {
                    maxim = num;
                }
            }
            return maxim;
        }

        static float Minims(List<float> temperatures)
        {
            float minim = temperatures[0];
            foreach (float num in temperatures)
            {
                if (num < minim)
                {
                    minim = num;
                }
            }
            return minim;
        }

        static void Altes(List<float> temperatures)
        {
            float valorMitja = Mitjana(temperatures);
            valorMitja *= 1.2f;

            foreach (float num in temperatures)
            {
                if (num > valorMitja)
                {
                    Console.WriteLine(num);
                }
            }
        }

        static void Baixes(List<float> temperatures)
        {
            float valorMitja = Mitjana(temperatures);
            valorMitja *= 0.7f;

            foreach (float num in temperatures)
            {
                if (num < valorMitja)
                {
                    Console.WriteLine(num);
                }
            }
        }

        static void AltesX(List<float> temperatures, string valor)
        {
            float valorMitja = Mitjana(temperatures);
            float numero = Convert.ToSingle(valor);
            valorMitja *= (numero / 100);

            foreach (float num in temperatures)
            {
                if (num < valorMitja)
                {
                    Console.WriteLine(num);
                }
            }
        }
        static void Farenheit(List<float> temperatures)
        {
            Console.WriteLine("La mitjana és de " + ((Mitjana(temperatures) - 273.15) * 1.8 + 32) + "ºF");
            Console.WriteLine("La temperatua maxima és " + ((Maxims(temperatures) - 273.15) * 1.8 + 32) + "ºF");
            Console.WriteLine("La temperatua minima és " + ((Minims(temperatures) - 273.15) * 1.8 + 32) + "ºF");
        }

        static void Kelvin(List<float> temperatures)
        {
            Console.WriteLine("La mitjana és de " + Mitjana(temperatures) + "ºK");
            Console.WriteLine("La temperatua maxima és " + Maxims(temperatures) + "ºK");
            Console.WriteLine("La temperatua minima és " + Minims(temperatures) + "ºK");
        }
    }
}
