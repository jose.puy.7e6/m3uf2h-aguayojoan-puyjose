﻿using System;

namespace Exercici3
{
    class Exercici3
    {

        public static void Main()
        {
            string reiniciar = "r";
            do
            {
                Console.Clear();
                LoOperation();
              
                reiniciar = Console.ReadLine();
            } while (reiniciar == "r") ;
            Console.ReadKey();
        }

        static void entraVal(ref int num1,ref int num2,ref int commandType) {
            Console.WriteLine("Por favor, introduzca el comando : ");
            string command = Console.ReadLine();
            Console.WriteLine("-------------------------------------\n");
            string[] commandSplitted = command.Split(" ");
            try
            {
                num1 = int.Parse(commandSplitted[1]);
                num2 = int.Parse(commandSplitted[2]);
            }
            catch (Exception e)
            {
                Console.WriteLine("------------ ERROR -----------------\n");
                Console.WriteLine("------------ Debe añadir dos valores a operar -----------------\n");

            }
            commandType = operationSelector(commandSplitted[0]);
        }
        static void LoOperation() {
            Console.WriteLine("\n-------------------- Calculator -----------------------\n");
            Console.WriteLine("Bienvenido al menú\n");
            Console.WriteLine("-------------------------------------\n");

            int commandType = -1;
            int num1 = 0;
            int num2 = 0;

            entraVal(ref num1, ref num2, ref commandType);

            int resultado = operatior(num1, num2, commandType);

            Console.WriteLine("El resultado de la operación es : " + resultado.ToString() + "\n");
            Console.WriteLine("-------------------- Calculator -----------------------\n");
            Console.WriteLine("Presione tecla r para reiniciar\n");

            
        }
        static int operationSelector(string operation)
        {
            switch (operation)
            {
                case "suma":
                    return 1;
                case "resta":
                    return 2;
                case "multiplica":
                    return 3;
                case "divide":
                    return 4;
                default:
                    return -1;
            }
        }
        static int operatior(int num1, int num2, int commandType)
        {
            switch (commandType)
            {
                case 1: return num1 + num2;
                case 2: return num1 - num2;
                case 3: return num1 * num2;
                case 4: return num1 / num2;
                default: return -1;
            }
        }
    }
}
