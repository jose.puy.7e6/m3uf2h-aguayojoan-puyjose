﻿using System;

namespace Exercici1
{
    class Temperatura
    {
        public static void Main()
        {
            double grades = 0.0;
            double gradesConverted = 0.0;

            Console.WriteLine("\nIntrodueix temperatura, la unitat de temperatura que es i la unitat a que vols convertir");
            string ent = Console.ReadLine();
            string[] text = ent.Split(" ");
            gradesConverted = conversor(text[0],text[1],text[2]);
            Console.WriteLine("\nLa temperatura en "+text[2]+" és");
            Console.WriteLine(gradesConverted);

        }
        static double conversor(string tempTxt, string unitStat, string unitEnd)
        {
            double temp = double.Parse(tempTxt);
            if (unitStat == "Celsius" && unitEnd == "Farenheit")
                return (temp * 1.8) + 32;
            if (unitStat == "Celsius" && unitEnd == "Kelvin")
                return temp + 273.15;
            if (unitStat == "Kelvin" && unitEnd == "Farenheit")
                return ((temp-273.15) * 1.8) + 32;
            if (unitStat == "Kelvin" && unitEnd == "Celsius")
                return temp - 273.15;
            if (unitStat == "Farenheit" && unitEnd == "Celsius")
                return (temp - 32) / 1.8;
            if (unitStat == "Farenheit" && unitEnd == "Kelvin")
                return ((temp - 32) / 1.8) + 273.15;
            Console.WriteLine("there has been an error");
            return -1;
        }
    }
}
