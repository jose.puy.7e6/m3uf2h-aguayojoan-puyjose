﻿using System;

namespace Exercici2
{
    class Program
    {
        static void Main()
        {
            float saldo = 0;
            float ingresMin = 100000;
            float ingresTotal = 0;
            int enIngresos = 0;
            float despesaMax = 0;
            float despesaTotal = 0;
            int enDespesas = 0;


            int input;
            do
            {
                Console.Clear();
                MostraMenu();
                input = Convert.ToInt32(Console.ReadLine());
                switch (input)
                {
                    case 1:
                        Ingres(ref saldo, ref ingresMin, ref ingresTotal, ref enIngresos);
                        break;
                    case 2:
                        Despesa(ref saldo, ref despesaMax, ref despesaTotal, ref enDespesas);
                        break;
                    case 3:
                        Estadistiques(despesaMax, despesaTotal, enDespesas, ingresMin, ingresTotal, enIngresos);
                        break;
                    case 4:
                        Visualitzar(ingresTotal, despesaTotal);
                        break;
                    case 5:
                        Impostos(ingresTotal);
                        break;
                }

            } while (input != 0);
        }

        static void MostraMenu()
        {
            Console.WriteLine("0 - Sortir");
            Console.WriteLine("1 - Afegir un ingrés");
            Console.WriteLine("2 - Afegir una despesa");
            Console.WriteLine("3 - Estadistiques");
            Console.WriteLine("4 - Visualització del Compte");
            Console.WriteLine("5 - Impostos");
        }

        static void Ingres(ref float saldo, ref float ingresMin, ref float ingresTotal, ref int enIngresos)
        {
            Console.WriteLine("Quant vols afegir?");
            double ent = Convert.ToDouble(Console.ReadLine());
            if (ent < ingresMin)
            {
                ingresMin = (float)ent;
            }
            saldo += (float)ent;
            ingresTotal += (float)ent;
            enIngresos++;
        }

        static void Despesa(ref float saldo, ref float despesaMax, ref float despesaTotal, ref int enDespesas)
        {
            Console.WriteLine("Quant vols restar?");
            double ent = Convert.ToDouble(Console.ReadLine());
            if (ent > despesaMax)
            {
                despesaMax = (float)ent;
            }
            saldo -= (float)ent;
            despesaTotal += (float)ent;
            enDespesas++;
            if (saldo < 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Comptabilitat al descobert, amb una descompensació de: " + saldo + "€");
            }
        }

        static void Estadistiques(float despesaMax, float despesaTotal, int enDespesas, float ingresMin, float ingresTotal, float enIngresos)
        {
            if (enDespesas > 0)
            {
                Console.WriteLine("La despesa més gran ha sigut de " + despesaMax + "€");
            }
            if (enIngresos > 0)
            {
                Console.WriteLine("L'ingrés més petit ha sigut de " + ingresMin + "€");
                Console.WriteLine("La mitjana d'ingressos és " + ingresTotal / enIngresos);
            }
            if (enDespesas > 0)
            {
                Console.WriteLine("La mitjana de despeses és " + despesaTotal / enDespesas);
            }
            Console.ReadLine();
        }

        static void Visualitzar(float despesaTotal, float ingresTotal)
        {
            Console.WriteLine("Les despeses totals han sigut de " + despesaTotal + "€");
            Console.WriteLine("Les despeses totals han sigut de " + ingresTotal + "€");
            Console.WriteLine("El total actual és de " + (ingresTotal - despesaTotal) + "€");
            Console.ReadLine();
        }

        static void Impostos(float ingresTotal)
        {
            float iva = ingresTotal * 0.21F;
            float irpf = (ingresTotal - iva) * 0.15f;
            float ingresosNets = ingresTotal - iva - irpf;

            Console.WriteLine("El total d'IVA a pagar és de " + iva + "€");
            Console.WriteLine("El total d'IRPF a pagar és de " + irpf + "€");
            Console.WriteLine("Els ingressos nets són " + ingresosNets + "€");
            Console.ReadLine();
        }
    }
}
